#include <iostream>
#include <string>

bool isPrime(int x) {
    return x == 2 || x == 3 || x == 5 || x == 7;
}

int main() {

    int x {};

    std::cout << "Enter a number between 0-9. I'll tell you if it's prime." << '\n';

    std::cin >> x;

    if (isPrime(x)) {
        std::cout << "It's prime!!";
    }
    else {
        std::cout << "NO WAY.";
    }

    return 0;
}