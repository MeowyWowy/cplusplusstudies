#include <iostream>
#include <string_view>

int getAge(std::string_view name) {
    int age {};
    std::cout << "What is the age of " << name << "?\n";
    std::cin >> age;
    return age;

}

std::string getName(int personNumber) {
    std::string name {};
    std::cout << "What is the name of person #" << personNumber << "\n";
    std::getline(std::cin >> std::ws, name);
    return name;
}

int main() {
    std::string personOne {getName(1)};
    int personOneAge {getAge(personOne)};

    std::string personTwo {getName(2)};
    int personTwoAge {getAge(personTwo)};

    if (personOneAge > personTwoAge) {
        std::cout << personOne << "is older than" << personTwo;
    }
    else {
        std::cout << personTwo << "is older than" << personOne;
    }


    return 0;
}