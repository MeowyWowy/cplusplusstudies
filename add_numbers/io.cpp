#include "io.h"
#include <iostream>

int readNumber() {
    int x;
    std::cout << "Please input a number: \n";
    std::cin >> x;
    return x;
}

void writeAnswer(int answer) {
    std::cout << "The answer is: " << answer;
}